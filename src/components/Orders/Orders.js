import React from 'react';


const Order = (props) => {

    const showOrders = () =>{
        let orderName = [];
        if (props.count > 0) {
            orderName.push(props.name)
        }
        return orderName;
    };

    return (
      showOrders().map((order, index)=> {
          return (
              <div key={index}>{order}</div>
          )
      })
    )
};

export default Order;