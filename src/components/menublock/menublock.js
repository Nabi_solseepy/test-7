import React from 'react';
import Orders from '../Orders/Orders';

const menuBlock = (props) => {
    return(
        <div>
            {props.menu.map((menu, index) => {
                return (
                    <Orders key={index} count={menu.count} name={menu.name}/>
                )
            })}
        </div>
    )
};
export default menuBlock;