import React from 'react';
import  './Buttons.css'
import Hamburger from '../../img/burger.png';
import cheeseburger from '../../img/cheeseburger.png';
import Fries from '../../img/icons8-french-fries-30.png';
import Coffe from '../../img/icons8-coffee-to-go-30.png';
import Tea from '../../img/icons8-tea-30.png';
import Cola from '../../img/cola.png';


const Сarte = [
    {name: 'Hamburger', price: 50 +' som', img:Hamburger},
    {name: 'Cheeseburger', price: 60 + ' som ',img:cheeseburger},
    {name: 'Fries', price: 5+ ' som',img:Fries},
    {name: 'Coffe', price: 70+ ' som',img:Coffe},
    {name: 'Tea', price: 25 + ' som',img:Tea},
    {name: 'Cola', price: 30+' som', img:Cola},
];
const menuCount = (props, ingredientName) => {
    let id = props.menu.findIndex((Menu) => {
        return Menu.name === ingredientName
    });
    return props.menu[id].count;
};
const getCountMenu =(props,ordersName) => {
    let id = props.Menu.findIndex((Menu) =>{
        return Menu.name === ordersName
    });
    return props.Menu[id].count;
};
const Buttons = (props) => {
    return (
        <div className="btn-item">
            {Сarte.map((menu, id) =>{
                return (
                    <div key={id} className="buttons">
                        <button className="btn" onClick={() => props.click(menu.name)} type="button"><img src={menu.img} alt=""/><p>{menu.name}</p>{menu.price}{Сarte.name}</button>
                        <span>{menuCount(props,menu.name)}</span><button onClick={(name) => props.onClick(menu.name)} className="btn-rm" type="button">x</button>
                    </div>
                )
            })}
        </div>


    )
};
export default Buttons;