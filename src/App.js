import React, { Component } from 'react';
import './App.css';


import Buttons from './components/Buttons/Buttons'
import Menublock from './components/menublock/menublock'


class App extends Component {
  state ={
    Menu: [
        {name: 'Hamburger', count: 0, id: 1},
        {name: 'Cheeseburger', count: 0,id:2},
        {name: 'Fries',  count: 0,id:3},
        {name: 'Coffe',count: 0,id:4},
        {name: 'Tea', count: 0, id:5},
        {name: 'Cola', count: 0, id:6},

    ]
  };

  addOrders = (name) => {
    let Menu = [...this.state.Menu];
    for (let i = 0; i < Menu.length; i++){
      if (name === Menu[i].name){
          Menu[i].count++
      }
    }
    this.setState({
        Menu: Menu
    });

  };
    removeOrders = (name)=> {
        let menu = [...this.state.Menu];
        for (let i = 0; i < menu.length; i++){
            if (name === menu[i].name && menu[i].count > 0){
                menu[i].count--
            }
        }
        this.setState({
            Menu: menu
        })
    };
  render() {
    return (
      <div className="App">

        <div className="Order-Details">

     <Menublock menu={this.state.Menu}/>
        </div>

        <div className="Order-Details">
            <Buttons menu={this.state.Menu} click={(name) => this.addOrders(name)} onClick={(name) => this.removeOrders(name)}/>
             <span>{}</span>
        </div>
      </div>

    );
  }

}

export default App;
